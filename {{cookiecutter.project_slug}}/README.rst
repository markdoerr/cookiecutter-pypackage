{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}

{{ cookiecutter.project_short_description }}

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `opensource/templates/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`opensource/templates/cookiecutter-pypackage`: https://gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage
