#!/usr/bin/env python
"""Tests for `{{ cookiecutter.project_slug }}` package."""
# pylint: disable=redefined-outer-name
from {{ cookiecutter.project_slug }} import __version__


def test_version():
    """Sample pytest test function."""
    assert __version__ == "{{ cookiecutter.version }}"
