.. _console-script-setup:


Console Script Setup
====================

Optionally, your package can include a console script using argparse (Python 3.2+).

To use the console script in development:

.. code-block:: bash

    pip install -e projectdir

'projectdir' should be the top level project directory with the pyproject.toml file

The script will be generated with output for no arguments and --help.

--help
    show help menu and exit
